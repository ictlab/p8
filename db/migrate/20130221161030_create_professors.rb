class CreateProfessors < ActiveRecord::Migration
  def change
    create_table :professors do |t|
      t.string :f_name
      t.string :l_name
    end
    create_table :courses_professors, :id => false do |t|
      t.references :course
      t.references :professor
    end
  end
end
