class Course < ActiveRecord::Base
  attr_accessible :name, :prof_array
  has_and_belongs_to_many :professors
  def prof_array
    self.professor_ids.join(",")
  end
  def prof_array=(str)
    self.professor_ids= str.split(",")
  end
end
