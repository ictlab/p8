class Professor < ActiveRecord::Base
  attr_accessible :f_name, :l_name, :name
  has_and_belongs_to_many :courses
  def name
    "#{self.f_name} #{self.l_name}"
  end
end
